### L3 internship

## Organization
├── bandits
│   ├── arm.py  
│   ├── boltzmann.py  
│   ├── comparegreedy.py  
│   ├── cusum.py  
│   ├── cusumtest.py  
│   ├── epsgreedy.py  
│   ├── evomeans.py  
│   ├── singlecontext.py  
│   ├── switches.py  
│   └── ucb.py  
├── consciousness  
│   ├── comparable.py  
│   ├── digit.py  
│   ├── main.py  
│   ├── mean_comparable.py  
│   ├── mean.py  
│   ├── mlp.py  
│   └── shape  
├── contextual_bandits
│   ├── arm.py  
│   ├── basic.py  
│   ├── believer.py  
│   ├── player.py  
│   ├── bigstats  
│   ├── bigstats2  
│   ├── generation.py  
│   ├── main.py  
│   ├── mlp.py  
│   ├── relevance.py  
├── rapport  
│   ├── annexes.tex  
│   ├── biblio.bib  
│   ├── domaine.tex  
│   ├── fin.tex  
│   ├── intro.tex  
│   ├── main.tex  
│   ├── socio.tex  
│   └── travail.tex  
└── README.md  



## Bandits

My implementation of multi armed bandits.

## Contextual Bandits

The algorithms done in bandits are put together in player, a better code but might be more difficult to understand at first.

## Consciousness

The experiment of the article of Cleermans


## Débutant

My mlp to learn my sinus with outliers

## Rapport

Coming soon.