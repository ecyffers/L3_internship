import numpy as np 


class boltzmann:
	'''Class for the boltzmann exploration,
	also known as softmax'''

	def __init__(self, tau, nb_arm, tau_fun=None):
		self.nb_arm = nb_arm

		self.tau = tau
		self.time = 0
		self.tau_fun = tau_fun

		self.means = np.zeros(nb_arm)
		self.chosen = np.zeros(nb_arm)

	def reset(self, zero = False):
		self.chosen = np.zeros(self.nb_arm)
		if zero:
			self.means = np.zeros(self.nb_arm)
		else:
			self.means = np.random.normal(0.5, 0.1, self.nb_arm)
		self.time = 0

	def selection(self, expected = False):
		'''a boltzmann distribution, with expected, we also have the expected value
		(used in change detection algorithm)'''
		proba = np.exp(self.means.copy()/self.tau)
		norm = proba.sum()
		proba/=norm

		arm =  np.random.choice([i for i in range(self.nb_arm)], p=proba)
		if expected:
			return self.means[arm], arm
		else :
			return arm
	def update(self, chosen_arm, reward):
		'''Update the data used by the algorithm 
		according to the result of the last arm pushed'''
		self.chosen[chosen_arm]+=1
		#The new empirical means : 
		self.means[chosen_arm]=((self.chosen[chosen_arm]-1)*self.means[chosen_arm]+reward)/self.chosen[chosen_arm]

		if self.tau_fun != None :
			self.tau = self.tau_fun(self.tau, self.time)
			self.time+=1


if __name__ == '__main__':
	import arm
	print("The softmax algorithm")
	myarm = arm.arm([[10, 0.5],[1,0.5]],[[12, 0.1], [6, 0.9]])
	mysoft = boltzmann(0.1, 2)
	for i in range(20):
		chosen_arm = mysoft.selection()
		reward = myarm.answer(chosen_arm)
		mysoft.update(chosen_arm, reward)
		print("We choose ", chosen_arm, " and with obtain ", reward, " with a tau of ", mysoft.tau)
