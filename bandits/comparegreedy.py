import numpy as np
import matplotlib.pyplot as plt


arms = [1.00,0.00]
# arms = [0.75,0.25]

runs = 250
trials = 100
reward = 1
epsilon = 0.1
gains = []

Z = np.zeros((runs, trials))

for run in range(runs):
    gain = 0
    counts = np.zeros(len(arms))
    values = np.random.normal(0.5,0.01, len(arms))
    for trial in range(trials):
        if np.random.uniform(0,1) < epsilon:
            arm = np.random.randint(len(arms))
        else:
            arm = np.argmax(values)

        # Was it the best arm ?
        if arm == np.argmax(arms):
            Z[run, trial] = 1
            
        reward = np.random.uniform(0,1) < arms[arm]
        gain += reward
        counts[arm] += 1
        count = counts[arm]
        value = values[arm]
        values[arm] = ((count - 1) / count) * value + (1 / count)*reward
        # Annealing
        # epsilon *= 0.999
    gains.append(gain)

    
print("Epsilon-greedy: %.2f +/- %.2f" % (np.mean(gains), np.std(gains)))

X = np.arange(trials)
M = np.mean(Z,axis=0)
V = np.var(Z,axis=0)



plt.figure(figsize=(8,5))
plt.title("Epsilon greedy ($\epsilon = 0.1$, $P = (%.2f, %.2f)$)" % (arms[0], arms[1]))
plt.plot(X, M, color="k", linewidth=2)
plt.fill_between(X, M+V, M-V, color="k", alpha=.1)
plt.ylim(0.0,1.1)
plt.xlabel("Trials")
plt.ylabel("Mean performance (best arm chosen)")
plt.show()