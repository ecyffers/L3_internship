import numpy as np 

class cusum:
	'''Class for the cusum algorithm;
	with the notation of Knowledge Discovery from
Data Streams'''

	def __init__(self, threshold, v):
		self.threshold = threshold
		self.v = v
		self.gt = 0
		self.current_context = 0

	def same_context(self, r):
		self.gt = max(0, self.gt + (r - self.v))
		if self.gt > self.threshold:
			#A change is detected
			self.gt = 0
			self.current_context = 1-self.current_context
			return False
		else :
			return True

	def set_belief(self, c):
		self.current_context = c
		self.gt = 0