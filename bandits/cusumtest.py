#A test of cusum algorithm,with the three classical methods of bandits

import numpy as np 

def gener(n):
	'''Return the seeds that will be used for each experiments'''
	np.random.seed(42)
	myseeds = []
	for i in range(n):
		myseeds.append(np.random.randint(n*100)) #maybe there is a better way to generate aleatoir but reproductible seeds
	return myseeds

def annealing(eps, t):
	return 1 / np.log(t + 3)


v = 0.5
threshold = 2
runs = 1000
trials = 100
alpha = 0.5

import arm
import cusum
import epsgreedy
import matplotlib.pyplot as plt


myarm = arm.arm([[1, 0.25],[1,0.75]], [[1,0.75],[1,0.25]])
greedy1 = epsgreedy.greedy(0.1,2)
greedy2 = epsgreedy.greedy(0.1,2)
mycusum = cusum.cusum(threshold,v)

myseeds = gener(runs)

choice_g = np.zeros((runs, trials))
belief = np.zeros((2,runs, trials))
correct = []

if __name__ == '__main__':
	for run in range(runs):
		np.random.seed(myseeds[run])
		greedy1.reset()
		greedy2.reset()
		myarm.set_context(0)
		mycusum.set_belief(0)

		for trial in range(trials):
			if trial ==50:
				myarm.set_context(1)

			if mycusum.current_context == 0 :
				belief[0][run][trial] = 1
				r, chosen_arm = greedy1.selection(expected = True)
				choice_g[run][trial]=chosen_arm
				reward = myarm.answer(chosen_arm)
				greedy1.update(chosen_arm, reward)
				mycusum.same_context(abs(reward-r))

			else:
				r, chosen_arm = greedy2.selection(expected = True)
				choice_g[run][trial]=chosen_arm
				reward = myarm.answer(chosen_arm)
				greedy2.update(chosen_arm, reward)
				mycusum.same_context(abs(reward-r))

				belief[1][run][trial] = 1


	#drawing part
	X = np.arange(trials)

	M = np.mean(choice_g,axis=0)
	V = np.var(choice_g,axis=0)

	plt.figure(figsize=(8,5))

	plt.title(myarm.presentation())
	plt.plot([50,50], [0,1], color = 'r', lw = 3, label = "Change of context")

	plt.plot(X, M, color="g", linewidth=2, label = "greedy")
	plt.fill_between(X, M+V, M-V, color="g", alpha=.2)
	plt.ylim(0.0,1.1)
	plt.xlabel("Trials")
	plt.ylabel("Mean performance (best arm chosen)")
	plt.legend()


	plt.figure(figsize = (10,5))


	plt.title("Belief of context with "+myarm.presentation())
	C1 = np.mean(belief[0], axis = 0)
	C2 = np.mean(belief[1], axis = 0)
	plt.plot(X, C1, color = 'b', label = "Context 1")
	plt.plot(X, C2, color = 'turquoise', label = "Context 2")

	plt.plot([50,50], [0,1], color = 'r', lw = 3, label = "Change of context")


	plt.show()