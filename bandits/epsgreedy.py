import numpy as np 


#various ideas of functions
def annealing(eps, t):
	return 1 / np.log(t + 3)

def expo(eps, t):
	return eps*np.exp(-t)

def lineareps(eps,t):
	return eps-t*0.0001

def geoeps(eps,t):
	return eps*0.999999


class greedy:
	'''class for the epsilon greedy algorithm'''

	def __init__(self, eps, nb_arm, eps_fun=None):
		self.epsilon =eps
		self.nb_arm = nb_arm

		#for the version with a decreasing epsilon
		self.eps_fun = eps_fun
		self.time = 0

		self.means = np.zeros(self.nb_arm)
		self.chosen = np.zeros(self.nb_arm)

	def reset(self, zero = False):
		self.chosen = np.zeros(self.nb_arm)
		self.means = np.random.normal(0.5, 0.01,self.nb_arm)
		if zero :
			self.means = np.zeros(self.nb_arm)
		self.time = 0

	def selection(self, expected = False):
		'''Select the arm to push : the best computed arm 
		with probability 1-eps, or a random one
		with expected, we also have the expected value
		(used in change detection algorithm)'''
		if expected :
			if np.random.uniform(0,1)<self.epsilon:
				arm = np.random.randint(self.nb_arm)
				return self.means[arm], arm
			else :
				arm = np.argmax(self.means)
				return self.means[arm], arm
		else :
			if np.random.uniform(0,1)<self.epsilon:
				return np.random.randint(self.nb_arm)
			else :
				return np.argmax(self.means)

	def update(self, chosen_arm, reward):
		'''Update the data used by the algorithm 
		according to the result of the last arm pushed'''
		self.chosen[chosen_arm]+=1
		old = self.means[chosen_arm]
		n = self.chosen[chosen_arm]
		#The new empirical means : 
		self.means[chosen_arm] = (old*(n-1) + reward)/n
		#If needed, the new epsilon
		if self.eps_fun != None :
			self.epsilon = self.eps_fun(self.epsilon, self.time)
			self.time+=1


if __name__ == '__main__':
	import arm
	import matplotlib.pyplot as plt
	print("A naive greedy algorithm")
	myarm = arm.arm([[1,1],[1,0]])
	mygreedy = greedy(0.1,2)

	runs = 250
	trials = 100
	Z = np.zeros((runs, trials))

	for run in range(runs):
		
		mygreedy.reset(zero = True)

		for trial in range(trials):
			chosen_arm = mygreedy.selection()
			reward = myarm.answer(chosen_arm)
			mygreedy.update(chosen_arm, reward)
			if chosen_arm == myarm.best_arm():
				Z[run,trial]=1
	X = np.arange(trials)
	M = np.mean(Z,axis=0)
	V = np.var(Z,axis=0)

	plt.figure(figsize=(8,5))
	#plt.title("Epsilon greedy ($\epsilon = 0.1$, $P = (%.2f, %.2f)$)" % (arms[0], arms[1]))
	plt.plot(X, M, color="k", linewidth=2)
	plt.fill_between(X, M+V, M-V, color="k", alpha=.1)
	plt.ylim(0.0,1.1)
	plt.xlabel("Trials")
	plt.ylabel("Mean performance (best arm chosen)")
	plt.show()