#Just to have some knowledges about the evolution of means
import numpy as np 

def gener(n):
	'''Return the seeds that will be used for each experiments'''
	np.random.seed(1)
	myseeds = []
	for i in range(n):
		myseeds.append(np.random.randint(n*100)) #maybe there is a better way to generate aleatoir but reproductible seeds
	return myseeds


if __name__ == '__main__':
	import arm
	import ucb
	import boltzmann
	import epsgreedy

	import matplotlib
	import matplotlib.pyplot as plt

	nb_exp = 100
	myseeds = gener(nb_exp)
	alpha = 0.5
	tmax = 1000



	mg0 = []
	mg1 = []
	er0 = []

	mygreedy = epsgreedy.greedy(0.1,2)
	mysoft = boltzmann.boltzmann(0.5,2)
	myucb = ucb.ucb(2)
	myarm = arm.arm([[1, 0.25],[1,0.75]], [[1,0.75], [1,0.25]])

	for i in range(nb_exp):
		np.random.seed(myseeds[i])
		mygreedy.reset()
		myarm.set_context(0)
		m_g0 = []
		m_g1 = []
		er = []
		for t in range(tmax):
			if t == 150 :
				myarm.set_context(1)
			expect, chosen_arm = mygreedy.selection(expected= True)
			reward = myarm.answer(chosen_arm)
			mygreedy.update(chosen_arm, reward)
			m_g0.append(mygreedy.means[0])
			m_g1.append(mygreedy.means[1])
			er.append(abs(expect-reward))

		mg0.append(m_g0)
		mg1.append(m_g1)
		er0.append(er)

	mg0 = np.array(mg0)
	mg1 = np.array(mg1)
	er0 = np.array(er0)

	mean0 = np.mean(mg0, axis = 0)
	mean1 = np.mean(mg1, axis = 0)
	meaner = np.mean(er0, axis = 0)
	s0 = np.std(mg0, axis = 0)
	s1 = np.std(mg1, axis = 0)
	s2 = np.std(er0, axis = 0)

	x = [i for i in range(tmax)]
	fig = plt.figure(figsize=(10,5))
	plt.title(myarm.presentation())
	plt.plot(x, mean0, color = 'g', label = "arm 0")
	plt.plot(x, mean1, color = 'r', label = "arm 1")
	plt.plot(x, meaner, color = 'b', label = "difference between expectation and reward")
	plt.fill_between(x, mean0-s0, mean0+s0, color = 'g', alpha = 0.2)
	plt.fill_between(x, mean1-s1, mean1+s1, color = 'r', alpha = 0.2)
	plt.fill_between(x, meaner-s2, meaner+ s2, color = 'b', alpha = 0.2)
	plt.legend()

	plt.show()