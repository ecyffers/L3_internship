# It reproduces the classic results of three algorithmes of bandits

import numpy as np 

def gener(n):
	'''Return the seeds that will be used for each experiments'''
	np.random.seed(1)
	myseeds = []
	for i in range(n):
		myseeds.append(np.random.randint(n*100)) #maybe there is a better way to generate aleatoir but reproductible seeds
	return myseeds



if __name__ == '__main__':
	import arm
	import ucb
	import boltzmann
	import epsgreedy

	import matplotlib
	import matplotlib.pyplot as plt

	nb_exp = 100
	myseeds = gener(nb_exp)
	alpha = 0.5
	tmax = 100

	moy_greedy = [0]*tmax
	moy_soft = [0]*tmax
	moy_ucb = [0]*tmax


	mygreedy = epsgreedy.greedy(0.1,2)
	mysoft = boltzmann.boltzmann(0.5,2)
	myucb = ucb.ucb(2)

	def run(moy_greedy, moy_soft, moy_ucb):
		for i in range(nb_exp):
			#we run the same conditions with the three algorithms
			choice_greedy = []
			choice_ucb = []
			choice_soft = []
			choice_opt = []
			#The greedy first
			#we fix the current seed
			np.random.seed(myseeds[i])
			mygreedy.reset()
			#we run the experience
			for t in range(tmax):
				chosen_arm = mygreedy.selection()
				choice_greedy.append(chosen_arm)
				reward = myarm.answer(chosen_arm)
				choice_opt.append(myarm.best_arm())
				mygreedy.update(chosen_arm, reward)

			#same for softmax
			np.random.seed(myseeds[i])
			mysoft.reset()
			for t in range(tmax):
				chosen_arm = mysoft.selection()
				choice_soft.append(chosen_arm)
				reward = myarm.answer(chosen_arm)
				mysoft.update(chosen_arm, reward)	

			#and for ucb
			np.random.seed(myseeds[i])
			myucb.reset()
			for t in range(tmax):
				chosen_arm = myucb.selection()
				choice_ucb.append(chosen_arm)
				reward = myarm.answer(chosen_arm)
				myucb.update(chosen_arm, reward)	


			#stock the result
			moy_greedy+=np.array(choice_greedy)
			moy_ucb+=np.array(choice_ucb)
			moy_soft+=np.array(choice_soft)

		#we do the mean
		moy_greedy=moy_greedy/nb_exp
		moy_soft=moy_soft/nb_exp
		moy_ucb=moy_ucb/nb_exp

		return moy_greedy, moy_soft, moy_ucb, choice_opt

	for j in range(5):
		alpha = 0.1*j
		myarm = arm.arm([[1, 0.5-alpha],[1,0.5+alpha]])
		#myarm = arm.arm([[1,0.75], [1+j,0.75]])
		
		moy_greedy, moy_soft, moy_ucb, choice_opt = run(moy_greedy, moy_soft, moy_ucb)
		

		#display
		x,y=[i for i in range(tmax)],choice_opt
		fig = plt.figure(figsize=(10,5))
		plt.title('Comparaison with a diiference of probability of '+str(round(alpha,2)))
		#plt.title('Comparaison with a difference of reward of '+'str(j+1)')
		plt.plot(x,y, color='black', lw = 2, label = 'best arm')

		"""
		#version point
		plt.scatter(x,moy_greedy, color = 'g', s = 10, marker ='x')
		plt.scatter(x, moy_soft, color = 'b', s = 10, marker ='+')
		plt.scatter(x, moy_ucb, color = 'r', s=10)
		"""
		plt.plot(x, moy_greedy, color = 'g', lw = 1, label = 'greedy')
		plt.plot(x, moy_soft, color = 'b', lw = 1, label = 'softmax')
		plt.plot(x, moy_ucb, color = 'r', lw = 1, label = 'ucb')

		plt.xlabel('Trials')
		plt.ylabel('Probabilty of good answers')
		plt.legend()
		fig.savefig('comp'+str(round(alpha,2))+'.png')
		#fig.savefig('magnitude'+str(j+1)+'.png')
	plt.show()
