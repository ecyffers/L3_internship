# It reproduces the classic results of three algorithmes of bandits

import numpy as np 

def gener(n):
	'''Return the seeds that will be used for each experiments'''
	np.random.seed(2)
	myseeds = []
	for i in range(n):
		myseeds.append(np.random.randint(n*100)) #maybe there is a better way to generate aleatoir but reproductible seeds
	return myseeds



if __name__ == '__main__':
	import arm
	import ucb
	import boltzmann
	import epsgreedy

	import matplotlib
	import matplotlib.pyplot as plt

	nb_exp = 500
	myseeds = gener(nb_exp)
	alpha = 0.5
	tmax = 300

	epsilon = 0.1
	tau = 0.1


	mygreedy = epsgreedy.greedy(epsilon,2)
	mysoft = boltzmann.boltzmann(tau,2)
	myucb = ucb.ucb(2)

	def run(moy_greedy, moy_soft, moy_ucb):
		for i in range(nb_exp):
			#we run the same conditions with the three algorithms
			choice_greedy = []
			choice_ucb = []
			choice_soft = []
			choice_opt = []
			#The greedy first
			#we fix the current seed
			np.random.seed(myseeds[i])
			mygreedy.reset(zero = True)
			myarm.set_context(0)
			#we run the experience
			for t in range(tmax):
				if t==100:
					myarm.change_context(1)
				chosen_arm = mygreedy.selection()
				choice_greedy.append(chosen_arm)
				reward = myarm.answer(chosen_arm)
				choice_opt.append(myarm.best_arm())
				mygreedy.update(chosen_arm, reward)

			#same for softmax
			np.random.seed(myseeds[i])
			mysoft.reset()
			myarm.set_context(0)
			for t in range(tmax):
				if t==100:
					myarm.change_context(1)
				chosen_arm = mysoft.selection()
				choice_soft.append(chosen_arm)
				reward = myarm.answer(chosen_arm)
				mysoft.update(chosen_arm, reward)	

			#and for ucb
			np.random.seed(myseeds[i])
			myucb.reset()
			myarm.set_context(0)

			for t in range(tmax):
				if t==100:
					myarm.change_context(1)
				chosen_arm = myucb.selection()
				choice_ucb.append(chosen_arm)
				reward = myarm.answer(chosen_arm)
				myucb.update(chosen_arm, reward)	


			#stock the result
			moy_greedy.append(np.array(choice_greedy))
			moy_ucb.append(choice_ucb)
			moy_soft.append(choice_soft)

		moy_greedy= np.array(moy_greedy)
		moy_soft = np.array(moy_soft)
		moy_ucb = np.array(moy_ucb)
		#we do the mean
		mean_greedy=np.mean(moy_greedy, axis = 0)
		mean_soft=np.mean(moy_soft, axis = 0)
		mean_ucb=np.mean(moy_ucb, axis = 0)

		e_greedy = np.var(moy_greedy, axis = 0)
		e_soft = np.var(moy_soft, axis =0)
		e_ucb = np.var(moy_ucb, axis =0)

		return mean_greedy, mean_soft, mean_ucb, choice_opt, e_greedy, e_soft, e_ucb



	myarm = arm.arm([[1, 0.5],[1,0]], [[1,0.5],[1,1]])
	#myarm = arm.arm([[1,0.75], [1+j,0.75]])
	
	moy_greedy, moy_soft, moy_ucb, choice_opt, e_greedy, e_soft, e_ucb = run([],[],[])
	

	#display
	x,y=[i for i in range(tmax)],choice_opt
	fig = plt.figure(figsize=(10,5))
	plt.title(myarm.presentation())
	#plt.title('Comparaison with a difference of reward of '+'str(j+1)')
	plt.plot(x,y, color='black', lw = 2, label = 'best arm')


	"""
	#version point
	plt.scatter(x,moy_greedy, color = 'g', s = 10, marker ='x')
	plt.scatter(x, moy_soft, color = 'b', s = 10, marker ='+')
	plt.scatter(x, moy_ucb, color = 'r', s=10)
	"""
	plt.plot(x, moy_greedy, color = 'g', lw = 1, label = 'greedy '+str(epsilon))
	plt.plot(x, moy_soft, color = 'b', lw = 1, label = 'softmax '+ str(tau))
	plt.plot(x, moy_ucb, color = 'r', lw = 1, label = 'ucb')
	
	plt.fill_between(x, moy_greedy - e_greedy, moy_greedy+e_greedy, color = 'g', alpha = 0.2)
	plt.fill_between(x, moy_soft - e_soft, moy_soft + e_soft, color = 'b', alpha =0.2)
	plt.fill_between(x, moy_ucb - e_ucb, moy_ucb + e_ucb, color = 'r', alpha =0.2)

	plt.xlabel('Trials')
	plt.ylabel('Mean of the chosen arm')
	plt.legend()
	fig.savefig('comp'+str(round(alpha,2))+'.png')
	#fig.savefig('magnitude'+str(j+1)+'.png')
	plt.show()
