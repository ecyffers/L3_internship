import numpy as np 

class ucb:
	'''class for the Upper confident bound algorithm'''

	def __init__(self, nb_arm):
		self.nb_arm = nb_arm

		self.time = 0
		self.means = np.zeros(nb_arm)
		self.chosen = np.zeros(nb_arm)

	def reset(self):
		#because we start with a play of each arm
		self.chosen = np.zeros(self.nb_arm)
		self.means = np.zeros(self.nb_arm)
		self.time=0

	def selection(self, expected = False):
		'''Select the arm to push
		with expected, we also have the expected value
		(used in change detection algorithm)'''
		if expected:
			if self.time < self.nb_arm :
				return 0,self.time
			else:
				to_play = self.means.copy()
				to_play+= np.sqrt(2*np.log(self.time)/self.chosen)
				arm =  np.argmax(to_play)
				return self.means[arm], arm
		else :
			if self.time < self.nb_arm :
				return self.time
			else:
				to_play = self.means.copy()
				to_play+= np.sqrt(2*np.log(self.time)/self.chosen)
				return np.argmax(to_play)
	def update(self, chosen_arm, reward):
		'''Update the data used by the algorithm 
		according to the result of the last arm pushed'''
		self.chosen[chosen_arm]+=1
		if self.time < self.nb_arm :
			self.means[chosen_arm]=reward
		else :
			#The new empirical means : 
			old = self.means[chosen_arm]
			n = self.chosen[chosen_arm]
			self.means[chosen_arm] = (old*(n-1) + reward)/n


		self.time+=1

if __name__ == '__main__':
	import arm
	print("The ucb algorithm")
	myarm = arm.arm([[10, 0.5],[1,0.5]],[[12, 0.1], [6, 0.9]])
	myucb = ucb(2)
	for i in range(20):
		chosen_arm = myucb.selection()
		reward = myarm.answer(chosen_arm)
		myucb.update(chosen_arm, reward)
		print("We choose ", chosen_arm, " and with obtain ", reward)

