import numpy as np
import mlp


if __name__ == '__main__':
    import matplotlib
    import matplotlib.pyplot as plt

    np.random.seed(5)
    epochs = 1000

    shape= [[1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1],
         [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1], 
         [1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1], 
         [1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1], 
         [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1], 
         [1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1], 
         [1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1], 
         [1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1], 
         [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1], 
         [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1]]
    answer = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0], 
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0], 
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]

    #The fstorder network that recognizes the digits
    fstorder = mlp.MLP(20,5,10)
    sndorder = mlp.MLP(5,5,35)

    efst = []
    esndid = []
    esnddigit = []
    esndshape = []
    esntot = []
    for i in range(epochs) :
        sumerror = 0
        errorshape = 0
        errorid = 0
        errordigit = 0
        errortot = 0

        for j in range(10):
            fstorder.propagate_forward(shape[j])
            error = fstorder.propagate_backward(answer[j], 0.1, 0.9)
            sndorder.propagate_forward(fstorder.layers[1])

            general = shape[j]+answer[j]+list(fstorder.layers[1])
            errorbis = sndorder.propagate_backward(general, 0.1,0.9, False)
            errorshape += (errorbis[0:20]**2).sum()
            errorid +=  (errorbis[20:25]**2).sum()
            errordigit += (errorbis[25:35]**2).sum()
            errortot += (errorbis**2).sum()

            sumerror+=error

        efst.append(np.sqrt(sumerror))
        esndid.append(np.sqrt(errorid))
        esndshape.append(np.sqrt(errorshape))
        esnddigit.append(np.sqrt(errordigit))
        esntot.append(np.sqrt(errortot))






    for i in range (10):
        output = fstorder.propagate_forward(shape[i])
        print (np.argmax(output))
        print(output)

    x = [i for i in range(epochs)]
    ref = efst/efst[0]
    ident = esndid/esndid[0]
    sha = esndshape/esndshape[0]
    dig = esnddigit/esnddigit[0]
    tot = esntot/esntot[0]
    plt.figure(figsize=(10,5))
    plt.plot(x,ref, color = 'r',lw = 2)
    plt.plot(x,sha,color = 'g', lw = 1)
    plt.plot(x,ident, color = 'y', lw = 1)
    plt.plot(x,dig, color = 'b', lw = 1)
    plt.plot(x,tot, color = 'turquoise', lw = 2)

    plt.show()

