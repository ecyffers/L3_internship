import numpy as np
import mlp


if __name__ == '__main__':
    import matplotlib
    import matplotlib.pyplot as plt

    np.random.seed(5)
    epochs = 1000

    shape= [[1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1],
         [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1], 
         [1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1], 
         [1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1], 
         [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1], 
         [1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1], 
         [1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1], 
         [1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1], 
         [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1], 
         [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1]]
    answer = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0], 
    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0], 
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1]]

    meanefst = [0]*epochs
    meanesnd = [0]*epochs
    meaneter = [0]*epochs

    for i in range(1000):
    #The fstorder network that recognizes the digits
        fstorder = mlp.MLP(20,5,10)
        sndorder = mlp.MLP(5,5,35)
        bigsndorder = mlp.MLP(5,10,35)

        efst = []
        esnd = []
        eter = []
        for i in range(epochs) :
            sumerror = 0
            sumerrorbis = 0
            sumerrorter = 0
            for j in range(10):
                fstorder.propagate_forward(shape[j])
                error = fstorder.propagate_backward(answer[j], 0.1, 0.9)
                sndorder.propagate_forward(fstorder.layers[1])
                bigsndorder.propagate_forward(fstorder.layers[1])


                general = shape[j]+answer[j]+list(fstorder.layers[1])
                errorbis = sndorder.propagate_backward(general, 0.1,0.9)
                general = shape[j]+answer[j]+list(fstorder.layers[1])
                errorter = bigsndorder.propagate_backward(general, 0.1, 0.9)
                sumerror+=error
                sumerrorbis+=errorbis
                sumerrorter+=errorter
            efst.append(np.sqrt(sumerror))
            esnd.append(np.sqrt(sumerrorbis))
            eter.append(np.sqrt(sumerrorter))
        meanefst+=np.array(efst)
        meanesnd+=np.array(esnd)
        meaneter+=np.array(eter)



    x,y = [i for i in range(epochs)], meanefst/meanefst[0]
    z = meanesnd/meanesnd[0]
    a = meaneter/meaneter[0]
    plt.figure(figsize=(10,5))
    plt.plot(x,y, color = 'g',lw = 1)
    plt.plot(x,z,color = 'r', lw = 1)
    plt.plot(x,a, color = 'b', lw = 1)
    plt.show()

