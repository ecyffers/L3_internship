import numpy as np 


class arm:
	'''The arm that will be used for every algorithm
	different context with Bernouilli arms'''
	def __init__(self, *args):
		'''Initialization of the contextual arm,
		with different contexts, arms and rewards (bernouilli bandit)'''
		self.univers = args
		self.number_context = len(args)#number of contexts
		self.number_arm = len(args[0])#number of arms (the same in all context)

		#Build the different contexts
		self.reward = [] #contains the different reward for each arm and context
		self.probability = [] #same with the probablity
		self.expected_value = [] #same with expected value
		for i in range(self.number_context):
			context_reward = []
			context_probability = []
			context_expected = []
			for j in range(self.number_arm) :
				context_reward.append(float(args[i][j][0]))
				context_probability.append(float(args[i][j][1]))
				context_expected.append(float(args[i][j][0]*args[i][j][1]))
			self.reward.append(context_reward)
			self.probability.append(context_probability)
			self.expected_value.append(context_expected)

		self.current_context = 0 #wlog

	def change_context(self, switch):
		'''Decide to change the context with probablity switch,
		the new context is randomly chosen'''
		if np.random.random()<switch: #we change
			shift = np.random.randint(self.number_arm-1) + 1
			self.current_context = (self.current_context+shift)%self.number_arm
	def set_context(self, context):
		'''Set the context to the argument given'''
		self.current_context = context
	def answer(self, chosen_arm):
		'''Return the result of pushing the chosen_arm'''
		if np.random.random()<self.probability[self.current_context][chosen_arm]:
			return self.reward[self.current_context][chosen_arm]
		else:
			return 0

	def best_arm(self) :
		'''Return the arm with the best expected value,
		which is the good theorical answer'''
		return np.argmax(self.expected_value[self.current_context])

	def presentation(self):
		me = str(self.number_arm)+" contexts : "
		for i in range(self.number_context) :
			me+="("
			for j in range(self.number_arm) :
				me+="arm " + str(j) +" : " +str(self.probability[i][j]*100) +"% " 
			me+=")\n"
		return me

if __name__ == '__main__':
	print("A nice try")

	myarm = arm([[10, 0.5], [1, 0.5]], [[12, 0.1], [6, 0.9]])
	for i in range(100):
		if i % 10 == 0:
			myarm.change_context(1)
		j = np.random.randint(2)
		ans = myarm.answer(j)
		print("we gain ", ans, "with ", j, "whereas the best arm is ", myarm.best_arm())




