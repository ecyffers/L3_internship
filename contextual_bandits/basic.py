import numpy as np 
import matplotlib.pyplot as plt

def gener(n):
	'''Return the seeds that will be used for each experiments'''
	np.random.seed(2)
	myseeds = []
	for i in range(n):
		myseeds.append(np.random.randint(n*100)) #maybe there is a better way to generate aleatoire but reproductible seeds
	return myseeds

def play(player, arm):
	chosen_arm = player.selection()
	reward = arm.answer(chosen_arm)
	player.update(chosen_arm, reward)
	return chosen_arm

def play_and_expect(player, arm):
	r, chosen_arm = player.believe_and_select()
	reward = arm.answer(chosen_arm)
	player.update(chosen_arm, reward)
	return r, reward, chosen_arm

def optimal(chosen_arm, arm):
	if chosen_arm == arm.best_arm():
		return 1
	else :
		return 0

def right_context(arm, context):
	if context == arm.current_context:
		return 1
	else:
		return 0

def experiment(runs, trials,seeds, arm, player):
	Z = np.zeros((runs, trials))
	for run in range(runs):
		np.random.seed(seeds[run])
		player.reset()
		for trial in range(trials):
			chosen_arm = play(player, arm)
			Z[run][trial]=optimal(chosen_arm, arm)
	return Z


def belief_and_play(arm, believer, player1, player2):
	if believer.current_context == 0:
		r, reward, chosen_arm = play_and_expect(player1, arm)
		c = believer.same_context(abs(reward-r))
	else :
		r, reward, chosen_arm = play_and_expect(player2, arm)
		c = believer.same_context(abs(reward-r))

	return c, r, chosen_arm, believer.current_context

def experiment_change(runs, trials, seeds, arm, believer, p1, p2, changes):
	Z = np.zeros((runs, trials))
	B = np.zeros((runs, trials))
	C = np.zeros((runs, trials))
	for run in range(runs):
		np.random.seed(seeds[run])
		arm.set_context(0)
		p1.reset()
		p2.reset()
		believer.set_belief(0)

		for trial in range(trials):
			if trial in changes:
				arm.set_context(1-arm.current_context)
			c, r, choice, context = belief_and_play(arm, believer, p1, p2)
			Z[run][trial] = optimal(choice, arm)
			B[run][trial] = right_context(arm, context)
			if not c :
				C[run][trial] = 1
	return Z, B, C


def drawing(X,Z, c = 'b', l = ""):
	M = np.mean(Z, axis = 0)
	V = np.var(Z, axis = 0)

	plt.plot(X, M, color=c, linewidth=1.5, label = l)
	plt.fill_between(X, M+V, M-V, color=c, alpha=.1)

if __name__ == '__main__':
	import arm
	import player
	import believer
	import matplotlib.pyplot as plt 

	runs = 2000
	trials = 96
	myseeds = gener(runs)
	tau = .1
	epsilon = .1
	alpha = .2
	changement = [24, 40, 48, 56, 64, 72, 80]


	myarm = arm.arm([[1, 0.25], [1, .75]], [[1, .75], [1, 0.25]])
	mycusum = believer.Cusum(1.7, .5)

	g1 = player.Epsgreedy(0.1,2)
	g2 = player.Epsgreedy(0.1,2)
	s1 = player.Boltzmann(0.1, 2)
	s2 = player.Boltzmann(0.1, 2)
	u1 = player.UCB(2)
	u2 = player.UCB(2)


	X = np.arange(trials)
	G, GB, _ =experiment_change(runs, trials, myseeds, myarm, mycusum, g1, g2, changement)
	S, SB, _ =experiment_change(runs, trials, myseeds, myarm, mycusum, s1, s2, changement)
	U, UB, _ =experiment_change(runs, trials, myseeds, myarm, mycusum, u1, u2, changement)
	#R, RB =experiment_change(runs, trials, myseeds, myarm, mycusum, r1, r2, changement)

	print("regret greedy :", np.mean(G))
	print("regret Boltzmann :", np.mean(S))
	print("regret UCB :", np.mean(U))
	plt.figure(figsize = (10,5))
	plt.title(myarm.presentation())

	#plt.title("Comparaison of the three algorithms with CUSUM as detector "+myarm.presentation())
	plt.ylim(-.1, 1.1)
	drawing(X, G, c='g', l = 'greedy '+str(epsilon))
	drawing(X, S, c='b', l = 'softmax '+str(tau))
	drawing(X, U, c='r', l = 'ucb')
	#drawing(X, R, c='c', l = 'reinforcement' +str(alpha))
	for x in changement:
		plt.plot([x,x], [0,1], color = 'black', lw = 3)
	plt.xlabel('Trials')
	plt.ylabel('Proportion of selection of the best arm')
	plt.legend()
	plt.tight_layout()
	plt.savefig("../courbes/truc.png")

	plt.figure(figsize = (10,5))
	plt.title(myarm.presentation())
	plt.ylabel('Proportion of right belief')
	drawing(X, GB, c = 'g', l = 'greedy '+str(epsilon))
	drawing(X, SB, c='b', l = 'softmax '+str(tau))
	drawing(X, UB, c='r', l = 'ucb')
	#drawing(X, RB, c= 'c', l = 'reinforcement'+str(alpha))
	for x in changement:
		plt.plot([x,x], [0,1], color = 'black',lw = 3)
	plt.xlabel('Trials')
	plt.ylim(-.1, 1.1)
	plt.legend()
	plt.tight_layout()
	plt.savefig("../courbes/bidule.png")
	plt.show()
