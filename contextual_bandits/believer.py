import numpy as np


class Believer(object):
    def __init__(self):
        self.current_context = 0

    def same_context(self, r):
        raise NotImplemented

    def set_belief(self, c):
        self.current_context = c


class Cusum(Believer):
    def __init__(self, threshold, delta):
        super().__init__()
        self.delta = delta
        self.gt = 0
        self.threshold = threshold

    def same_context(self, r):
        self.gt = max(0, self.gt + (r - self.delta))
        if self.gt > self.threshold:
            # A change is detected
            self.gt = 0
            self.current_context = 1-self.current_context
            return False
        else:
            return True

    def set_belief(self, *args):
        super().set_belief(*args)
        self.gt = 0


class Page(Believer):
    def __init__(self, threshold, delta, *args):
        self.current_context = 0
        self.delta = delta
        self.threshold = threshold
        self.cumul = 0
        self.mini = delta*2

    def change_context(self):
        self.current_context = 1-self.current_context
        self.cumul = 0
        self.mini = self.delta*2

    def same_context(self, r):
        self.cumul += r-self.delta
        self.mini = min(self.mini, self.cumul)
        if self.cumul - self.mini > self.threshold :
            self.change_context()
            return False
        else:
            return True

    def set_belief(self, c):
        self.current_context = c
        self.cumul = 0
        self.mini = self.delta*2


class Dynamic(Believer):
    def __init__(self, variance):
        self.current_context=0
        self.variance = variance

    def change_context(self):
        self.current_context = 1 - self.current_context

    def same_context(self, r):
        if r > self.variance:
            self.change_context()
            return False
        else:
            return True

    def set_belief(self, c):
        self.current_context = c
