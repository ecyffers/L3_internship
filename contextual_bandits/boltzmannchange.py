import numpy as np 
import arm
import player
import basic
import believer
import matplotlib.pyplot as plt


if __name__ == '__main__':
	runs = 2000
	trials = 300
	myseeds = basic.gener(runs)
	temperature = [.5, .2, .1, .05,]
	changement = [75, 150, 225]

	nb_arm =2

	alpha = [.5, .25, .15, .10,]
	#alpha = [.5]
	threshold = [.5, .75, .87, 1,1.25,  1.5]

	delta = .5 #this value is the most logical I believe

	for tau in temperature:
		print (" pour espsilon : ", e)
		for a in alpha :
			for th in threshold : 
				myarm = arm.arm([[1, .5 - a],[1,.5 +a]], [[1,.5 +a],[1,.5-a]])
				mycusum = believer.Page(th, delta)

				g1 = player.Boltzmann(tau, nb_arm)
				g2 = player.Boltzmann(tau, nb_arm)


				X = np.arange(trials)
				#all the data in them : 1 if and only if good arm, and if and only if good context
				B, BB, BC =basic.experiment_change(runs, trials, myseeds, myarm, mycusum, g1, g2, changement)
				score = np.mean(B)
				print("alpha  {0:.2F} et threshold {1:.2F} donnent {2:.2F}".format(a, th, score))
			
			print(" ----------")
#Drawings :
"""
		fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize = (10,8),
									gridspec_kw = {'height_ratios': [7, 2, 1]})

		# Individual trials (ax1)
		for i in range(trials):
			X, Y = [], []
			XC, YC = [], []
			for j in range(runs):
				if B[j][i] == 1:
					Y.append(j)
					X.append(i)
				if BC[j][i] == 1:
					YC.append(j)
					XC.append(i)
			ax1.scatter(X,Y, color = 'gray', edgecolor = 'none', alpha = '.5')
			ax1.scatter(XC, YC, facecolor = 'none', edgecolor = 'red')

		ax1.set_title("Contextual Bandit Task\n", fontsize=15)
		ax1.set_ylabel("Individual trials")
		ax1.set_xlim(0,trials)
		ax1.set_ylim(0,runs)




		#Reward
		X = np.arange(trials)
		M = np.mean(B, axis=0)
		V = np.var(B, axis=0)
		ax2.plot(X, M)
		ax2.fill_between(X, M+V,M-V, alpha=0.1)
		ax2.spines['right'].set_visible(False)
		ax2.spines['top'].set_visible(False)
		ax2.spines['bottom'].set_visible(False)
		ax2.spines['left'].set_position(('data', -2))
		ax2.axhline(0.5, color="black", linewidth=0.5, linestyle="--")
		ax2.set_xlim(0,trials)
		ax2.set_ylim(0,1)
		ax2.set_yticks([])
		ax2.set_xticks([])
		ax2.set_ylabel("Reward")


		#Context

		M = np.mean(BC, axis=0)
		V = np.var(BC, axis=0)
		ax3.plot(X, M)
		for x in changement:
			plt.plot([x,x], [0,1], color = 'black',lw = 3)

		ax3.fill_between(X, M+V,M-V, alpha=0.1)
		ax3.spines['right'].set_visible(False)
		ax3.spines['top'].set_visible(False)
		ax3.spines['bottom'].set_visible(False)
		ax3.spines['left'].set_position(('data', -2))
		ax3.axhline(0.5, color="black", linewidth=0.5, linestyle="--")
		ax3.set_xlim(0,trials)
		ax3.set_ylim(0,1)
		ax3.set_yticks([])
		ax3.set_xticks([])
		ax3.set_ylabel("Switches")


		plt.tight_layout()#prettier
		#plt.savefig("../courbes/firstchangedetectionformat.png")

	plt.show()

"""