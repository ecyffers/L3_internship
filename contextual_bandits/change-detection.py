# -----------------------------------------------------------------------------
# Change detection in contextual multi-armed bandit task
# Copyright 2018 (C) Edwige Cyffers & Nicolas P. Rougier
# Released uner a BSD two-clauses license
# -----------------------------------------------------------------------------

# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

# Parameters
n_runs   = 100
n_trials = 200
P1, P2   = 0.65, 0.35
switches = [0, 1*n_trials//4, 2*n_trials//4, 3*n_trials//4, n_trials]
contexts = [(P1,P2), (P2,P1), (P1,P2), (P2,P1)]
seed = np.random.randint(0,1000)
np.random.seed(seed)
print("Random seed: {}".format(seed))


# Play first arm (irrespectively of context) and
#  collect rewards for all runs & trials
R = np.zeros((n_runs, n_trials))
for i,(t0,t1) in enumerate(zip(switches[:-1], switches[1:])):
    R_ = R[:,t0:t1]
    R_[...] = np.random.uniform(0, 1, R_.shape) < contexts[i][0]


# Totally fake context detection
CS = np.zeros((n_runs, n_trials))
for t in switches[1:-1]:
    T = t + np.abs(np.random.normal(0, 5, n_runs))
    for i in range(n_runs):
        CS[i, int(T[i])] = 1


# Display results
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(10,8),
                                    gridspec_kw = {'height_ratios':[8, 1, 1]})

# Individual trials (ax1)
X = np.tile(np.arange(n_trials), n_runs)
print("X : ", X)
Y = np.repeat(np.arange(n_runs), n_trials)
print("Y: ", Y)
FC = 0.75 + 0.25*np.ones((n_trials*n_runs,3))*(1-R.reshape(n_trials*n_runs,1))
EC = np.zeros((n_trials*n_runs,4))
EC[CS.ravel() > 0, 3] = 1
print(FC)


ax1.scatter(X, Y, s=9, linewidth=.75, edgecolor=EC,facecolor=FC, clip_on=False)
#ax1.spines['right'].set_visible(False)
#ax1.spines['top'].set_visible(False)
#ax1.spines['bottom'].set_visible(False)
#ax1.spines['left'].set_position(('data', -2))
ax1.set_xlim(0,n_trials)
ax1.set_xticks([])
ax1.set_ylim(0,n_runs)
#ax1.set_yticks([])
ax1.set_title("Contextual Bandit Task\n", fontsize=15)
ax1.text(0.5, 1.025, "C_1=({0},{1}), C_2=({1},{0})".format(P1,P2),
         ha="center", va="bottom", transform=ax1.transAxes, fontsize=10)
ax1.set_ylabel("Individual trials")


# Mean reward (ax2)
X = np.arange(n_trials)
M = np.mean(R, axis=0)
V = np.var(R, axis=0)
ax2.plot(X, M)
ax2.fill_between(X, M+V,M-V, alpha=0.1)
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.spines['bottom'].set_visible(False)
ax2.spines['left'].set_position(('data', -2))
ax2.axhline(0.5, color="black", linewidth=0.5, linestyle="--")
ax2.set_xlim(0,n_trials)
ax2.set_ylim(0,1)
ax2.set_yticks([])
ax2.set_xticks([])
ax2.set_ylabel("Reward")


# Switches (ax3)

X = np.arange(n_trials)
M = np.mean(CS, axis=0)
V = np.var(CS, axis=0)
ax3.plot(X, M, color="black")
ax3.fill_between(X, M+V,M-V, color="black", alpha=0.1)

ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax3.spines['left'].set_position(('data', -2))
ax3.spines['bottom'].set_position(('data', -0.1))

for switch in switches[1:-1]:
    ax3.axvline(switch, color="red", alpha=1.0, linewidth=1.5, zorder=10)
ax3.set_xlim(0,n_trials)
# ax3.set_ylim(0,1)
ax3.set_yticks([])
ax3.set_xlabel("Trials")
ax3.set_ylabel("Switches")


plt.tight_layout()
plt.savefig("change-detection.pdf")
plt.show()
