import numpy as np 
import matplotlib.pyplot as plt

if __name__ == '__main__':
	import basic
	import arm
	import player
	import believer

	runs = 2000
	trials = 96
	myseeds = basic.gener(runs)
	tau = .1
	epsilon = .1
	alpha = .2
	changement = [24, 40, 48, 56, 64, 72, 80]


	regretG = []
	regretS = []
	regretU = []
	th = []

	myarm = arm.arm([[1, 0.25], [1, .75]], [[1, .75], [1, 0.25]])

	for i in range (10):
		seuil = .6 + .2*i
		th.append(seuil)
		mycusum = believer.Page(seuil, .5)

		g1 = player.Epsgreedy(0.1,2)
		g2 = player.Epsgreedy(0.1,2)
		s1 = player.Boltzmann(0.1, 2)
		s2 = player.Boltzmann(0.1, 2)
		u1 = player.UCB(2)
		u2 = player.UCB(2)


		X = np.arange(trials)
		G, _, _ =basic.experiment_change(runs, trials, myseeds, myarm, mycusum, g1, g2, changement)
		S, _, _ =basic.experiment_change(runs, trials, myseeds, myarm, mycusum, s1, s2, changement)
		U, _, _ =basic.experiment_change(runs, trials, myseeds, myarm, mycusum, u1, u2, changement)
		
		regretG.append(np.mean(G))
		regretS.append(np.mean(S))
		regretU.append(np.mean(U))

	print(regretG)
	print(regretS)
	print(regretU)

	plt.figure(figsize = (10,5))
	plt.title(myarm.presentation())
	plt.plot(th, regretG, color='g', linewidth=1.5, label = 'greedy')
	plt.plot(th, regretS, color='b', linewidth=1.5, label='softmax')
	plt.plot(th, regretU, color='r', linewidth=1.5, label='UCB')

	plt.xlabel('Threshold')
	plt.ylabel('Regret')
	plt.legend()
	plt.tight_layout()
	plt.savefig("../courbes/truc.png")
