#Define the class with the different algorithmes to solve the contextual bandits problem
import numpy as np 

class DPlayer(object):
	def __init__(self, nb_arm, short_memory):
		self.nb_arm = nb_arm
		self.time = 0
		self.smem = short_memory
		self.global_mean = np.random.normal(0.5, 0.01, self.nb_arm)
		self.current_mean = np.random.normal(0.5, 0.01, self.nb_arm)
		self.chosen = np.zeros(self.nb_arm)

	def reset(self, zero=False):
		self.chosen = np.zeros(self.nb_arm)
		self.global_mean = np.random.normal(0.5, 0.01, self.nb_arm)
		self.current_mean = np.random.normal(0.5, 0.01, self.nb_arm)
		if zero:
			self.global_mean = np.zeros(self.nb_arm)
		self.time = 0

	def selection(self):
		raise NotImplementedError()

	def believe_and_select(self):
		raise NotImplementedError()

	def update(self, chosen_arm, reward):
		'''Update the data used by the algorithm
		according to the result of the last arm pushed'''
		self.chosen[chosen_arm] += 1
		old = self.global_mean[chosen_arm]
		n = self.chosen[chosen_arm]
		# The new empirical global_mean :
		self.global_mean[chosen_arm] = (old*(n-1) + reward)/n
		self.time += 1
		# The current mean:
		previous = self.current_mean[chosen_arm]
		if n <= self.smem:  # We are at the beginning,
			# so it is the same as the global one
			self.current_mean[chosen_arm] = (old*(n-1) + reward)/n
		else:
			self.current_mean[chosen_arm] = (previous*(self.smem-1)+reward)/self.smem


class DEpsgreedy(DPlayer):
	'''class for the epsilon greedy algorithm'''

	def __init__(self, epsilon, *args, eps_fun=None):
		super().__init__(*args)
		self.epsilon = epsilon
		self.eps_fun = eps_fun

	def selection(self):
		if np.random.uniform(0,1)<self.epsilon:
			arm = np.random.randint(self.nb_arm)
		else:
			arm = np.argmax(self.global_mean)
		return arm

	def believe_and_select(self):
		arm = DEpsgreedy.selection(self)
		return self.global_mean[arm], self.current_mean[arm], arm

	def update(self, *args):
		super().update(*args)
		#If needed, the new epsilon
		if self.eps_fun is not None :
			self.epsilon = self.eps_fun(self.epsilon, self.time)


class DBoltzmann(DPlayer):
	'''Class for the boltzmann exploration,
	also known as softmax'''
	def __init__(self, tau, *args, tau_fun = None):
		super().__init__(*args)
		self.tau = tau 
		self.tau_fun = tau_fun

	def selection(self):
		proba = np.exp(self.global_mean.copy()/self.tau)
		norm = proba.sum()
		proba/=norm

		arm =  np.random.choice([i for i in range(self.nb_arm)], p=proba)
		return arm

	def believe_and_select(self):
		arm = DBoltzmann.selection(self)
		return self.global_mean[arm], self.current_mean[arm], arm

	def update(self, *args):
		super().update(*args)
		if self.tau_fun is not None :
			self.tau = self.tau_fun(self.tau, self.time)


class DUCB(DPlayer):
	'''class for the Upper confident bound algorithm'''

	def reset(self):
		super().reset()


	def selection(self):
		if self.time < self.nb_arm :
			return self.time
		else:
			to_play = self.global_mean.copy()
			to_play+= np.sqrt(2*np.log(self.time)/self.chosen)
			return np.argmax(to_play)

	def believe_and_select(self):
		arm = DUCB.selection(self)
		return self.global_mean[arm], self.current_mean[arm], arm

	def update(self, chosen_arm, reward):
		if self.time < self.nb_arm:
			self.global_mean[chosen_arm]=reward
			self.current_mean[chosen_arm]=reward
			self.time+=1
			self.chosen[chosen_arm]=1
		else:
			super().update(chosen_arm, reward)
