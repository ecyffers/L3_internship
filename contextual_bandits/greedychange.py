import numpy as np 
import arm
import player
import basic
import believer
import matplotlib.pyplot as plt


if __name__ == '__main__':
	runs = 100
	trials = 300
	myseeds = basic.gener(runs)
	espsilons = [.5, .2, .1, .05,]
	changement = [75, 150, 225]

	nb_arm =2

	alpha = [.5, .25, .15, .10,]
	a=.3
	threshold = [.5, .75, .87, 1,1.25,  1.5]
	th=.87
	delta = .5 #this value is the most logical I believe

	e=.1
	"""
	for e in espsilons :
		for th in threshold :
			for a in alpha :
				"""
	myarm = arm.arm([[1, .5 - a],[1,.5 +a]], [[1,.5 +a],[1,.5-a]])
	mycusum = believer.Cusum(th, delta)

	g1 = player.Epsgreedy(e, nb_arm)
	g2 = player.Epsgreedy(e,nb_arm)


	X = np.arange(trials)
	#all the data in them : 1 if and only if good arm, and if and only if good context
	G, GB, GC =basic.experiment_change(runs, trials, myseeds, myarm, mycusum, g1, g2, changement)
	score = np.mean(G)
	print("alpha  {0:.2F} et threshold {1:.2F} donnent {2:.2F}".format(a, th, score))


#Drawings :

	fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize = (10,8),
								gridspec_kw = {'height_ratios': [7, 2, 1]})

	# Individual trials (ax1)
	for i in range(trials):
		X, Y = [], []
		XC, YC = [], []
		for j in range(runs):
			if G[j][i] == 1:
				Y.append(j)
				X.append(i)
			if GC[j][i] == 1:
				YC.append(j)
				XC.append(i)
		ax1.scatter(X, Y, s=5, color = 'gray', edgecolor = 'none', alpha = '.5')
		ax1.scatter(XC, YC, s=15, facecolor = 'none', edgecolor = 'red')

	ax1.set_title("Contextual Bandit Task\n", fontsize=15)
	ax1.set_ylabel("Individual trials", fontsize=15)
	ax1.set_xlim(0,trials)
	ax1.set_ylim(0,runs)




	#Reward
	X = np.arange(trials)
	M = np.mean(G, axis=0)
	V = np.var(G, axis=0)
	ax2.plot(X, M)
	ax2.fill_between(X, M+V,M-V, alpha=0.1)
	ax2.spines['right'].set_visible(False)
	ax2.spines['top'].set_visible(False)
	ax2.spines['bottom'].set_visible(False)
	ax2.spines['left'].set_position(('data', -2))
	ax2.axhline(0.5, color="black", linewidth=0.5, linestyle="--")
	ax2.set_xlim(0,trials)
	ax2.set_ylim(0,1)
	ax2.set_yticks([])
	ax2.set_xticks([])
	ax2.set_ylabel("Reward", fontsize=15)


	#Context

	M = np.mean(GC, axis=0)
	V = np.var(GC, axis=0)
	ax3.plot(X, M)
	for x in changement:
		plt.plot([x,x], [0,1], color = 'black',lw = 3)

	ax3.fill_between(X, M+V,M-V, alpha=0.1)
	ax3.spines['right'].set_visible(False)
	ax3.spines['top'].set_visible(False)
	ax3.spines['bottom'].set_visible(False)
	ax3.spines['left'].set_position(('data', -2))
	ax3.axhline(0.5, color="black", linewidth=0.5, linestyle="--")
	ax3.set_xlim(0,trials)
	ax3.set_ylim(0,1)
	ax3.set_yticks([])
	ax3.set_xticks([])
	ax3.set_ylabel("Switches", fontsize=15)


	plt.tight_layout()#prettier
	plt.savefig("../courbes/greedy(" +str(e)+")th("+str(th)+")a(" +str(a)+").png")

	plt.show()

