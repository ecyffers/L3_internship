if __name__ == '__main__' :
	import numpy as np 
	import arm
	import player
	import basic

	import matplotlib.pyplot as plt 

	runs = 5000
	trials = 50
	myseeds = basic.gener(runs)
	tau = .05
	epsilon = .1
	alpha = .2
	nb_arm = 2

	g1 = player.Epsgreedy(epsilon,nb_arm)
	s1 = player.Boltzmann(tau, nb_arm)
	u1 = player.UCB(nb_arm)

	myarm = arm.arm([[1, 1],
		[1, 0]])


	X = np.arange(trials)
	G =basic.experiment(runs, trials, myseeds, myarm, g1)
	S =basic.experiment(runs, trials, myseeds, myarm, s1)
	U =basic.experiment(runs, trials, myseeds, myarm, u1)

	plt.figure(figsize = (10, 5))
	plt.title("Problème des bandits : arm 1 : 100%, arm 2 : 0%", fontsize = 35)
	plt.ylim(-.1, 1.1)
	plt.xlabel('Trials', fontsize = 35)
	plt.ylabel('Proportion of selection of the best arm', fontsize = 35)
	basic.drawing(X, G, c='g', l = 'greedy '+str(epsilon))
	basic.drawing(X, S, c='b', l = 'softmax '+str(tau))
	basic.drawing(X, U, c='r', l = 'ucb')
	plt.legend()
	plt.show()
