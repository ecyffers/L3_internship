ucb accuracy with 2000 runs and change at 75, 150 and 225

alpha  0.50 et threshold 0.50 donnent 0.93
alpha  0.50 et threshold 0.75 donnent 0.93
alpha  0.50 et threshold 0.87 donnent 0.93
alpha  0.50 et threshold 1.00 donnent 0.92
alpha  0.50 et threshold 1.25 donnent 0.92
alpha  0.50 et threshold 1.50 donnent 0.93
 ----------
alpha  0.25 et threshold 0.50 donnent 0.81
alpha  0.25 et threshold 0.75 donnent 0.82
alpha  0.25 et threshold 0.87 donnent 0.82
alpha  0.25 et threshold 1.00 donnent 0.82
alpha  0.25 et threshold 1.25 donnent 0.82
alpha  0.25 et threshold 1.50 donnent 0.82
 ----------
alpha  0.15 et threshold 0.50 donnent 0.73
alpha  0.15 et threshold 0.75 donnent 0.73
alpha  0.15 et threshold 0.87 donnent 0.73
alpha  0.15 et threshold 1.00 donnent 0.73
alpha  0.15 et threshold 1.25 donnent 0.73
alpha  0.15 et threshold 1.50 donnent 0.73
 ----------
alpha  0.10 et threshold 0.50 donnent 0.66
alpha  0.10 et threshold 0.75 donnent 0.66
alpha  0.10 et threshold 0.87 donnent 0.66
alpha  0.10 et threshold 1.00 donnent 0.66
alpha  0.10 et threshold 1.25 donnent 0.67
alpha  0.10 et threshold 1.50 donnent 0.67
 ----------
