#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np

def sinus(x):
    return np.sin(x*2*np.pi)

def lure(): #it is a radom point but we can do more sophisticated ideas later
    '''Return a outlier '''
    return (2*np.random.random()-1)

def lure_shift(x, goal, shift=0.6):
    eps = 2*np.random.randint(2)-1
    value = goal(x)+shift*eps #the outlier is distinct of shift from the real curve
    return ((value+1)%2)-1

def gen_samples_proba(points, goal, mistakes) :
    ''' Return a sample for the input with %mistakes false points'''
    outlier=[]
    samples = np.zeros(points, dtype=[('x',  float, 1), ('y', float, 1)])
    samples['x'] = np.random.random(points)
    for i in range(points):
        isamistake = np.random.random()
        if isamistake < mistakes: # it is an outlier
            samples['y'][i] = lure_shift(samples['x'][i], sinus)
            #samples['y'][i] = lure()
            outlier.append(i)
        else: #the good point
            samples['y'][i] = goal(samples['x'][i])
    return samples, outlier


def gen_samples_number(points, goal, mistakes) :
    ''' Return a sample for the input with mistakes false points'''
    samples = np.zeros(points, dtype=[('x',  float, 1), ('y', float, 1)])
    samples['x'] = np.random.random(points)
    for i in range(points): #as we use random points, there is no problem to have all wrong points at the beginning ?
        if i< mistakes: # it is an outlier
            samples['y'][i] = lure()
        else: #the good point
            samples['y'][i] = goal(samples['x'][i])
    return samples

if __name__ == '__main__':
    import matplotlib
    import matplotlib.pyplot as plt
    def sinus(x):
        return np.sin(x*np.pi)
    samples = gen_samples_proba(800, sinus, 0)
    x,y = samples['x'],samples['y']
    plt.scatter(x,y,lw=0.01)
    plt.axis([0,1,0,1])
    plt.show()


#we can imagine all the function with 100 points, but you need to wait for 1000 to have the impression of a curve



