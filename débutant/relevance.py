#!/usr/bin/env python

import numpy as np
import mlp

def quadratic_error(network, samples, mem_outlier):
    ''' Return the quadratic difference between the output of the network and the goal'''
    quader = 0
    for i in range(samples.shape[0]):
        correct_point = samples['y'][i]
        computed_point = mem_outlier.get(i, 1000)
        if computed_point==1000:
            computed_point = float(network.propagate_forward(samples['x'][i]))
        quader+= (computed_point - correct_point)**2
    return np.sqrt(quader)

def isanoutlier(n,error, outlier, true_detection,false_detection, unseen, threshold):
    '''Decide if n in an outlier if its error is above the threshold, and update stats'''
    if n in outlier :
        if abs(error)>threshold:
            true_detection+=1
        else :
            unseen+=1
    else:
        if abs(error)>threshold:
            false_detection+=1
    return  true_detection,false_detection, unseen

def mustbeanoutlier(n,error,moy,outlier,true_detection,false_detection,unseen, factor):
    '''Same fonction but the detection depends of the factor and the curent mean of error'''
    if abs(error)> moy*factor:
        if n in outlier :
            true_detection+=1
        else :
            false_detection+=1
    else:
        if n in outlier :
            unseen+=1
        moy=(19*moy + abs(error))/20#not very elegant but easy to trust
    return true_detection, false_detection, unseen, moy


def stats_outlier(outlier, period,batch, true_detection, false_detection,unseen):
    total = period*batch
    print 'We had %d points with %d outliers' % (total , len(outlier))
    print 'We found %d ' % true_detection
    print 'But there is  %d false detections' % false_detection
    print 'And we miss %d points' % unseen

