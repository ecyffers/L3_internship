import numpy as np
import matplotlib.pyplot as plt


X = np.arange(80)
Y = []

for i in range(30):
	Y.append(np.random.normal(1, 0.2))

for i in range(30):
	Y.append(np.random.normal(2, 0.2))

for i in range(20):
	Y.append(np.random.normal(2, 0.5))


plt.plot(X, Y)
changement = [30, 60]
for x in changement:
	plt.plot([x,x], [0,4], color = 'black',lw = 3)

plt.show()
