% Une ou plusieurs sections présentant précisément le domaine de recherche :
% étude bibliographique (cette étude ne doit correspondre qu'à des articles et
% documents que vous avez effectivement lus), présentation précise des
% notions, définitions formelles et notations, ainsi que des résultats
% antérieurs.

%\subsection{Réseaux de neurones}
	
%	Introduire plein de trucs globaux de machine learning. Le MLP, les SOM
%	Le livre pour l'hstorique \cite{Goodfellow-et-al-2016}
%	Le livre du groupe de lecture : trop vulgarisation
%	L'article sur les SOM \cite{rougier:article:DSOM}
%	Sur les batch \cite{WILSON20031429}


	\subsection{Présentation des bandits manchots}

		Le problème des bandits manchots tire son nom du problème suivant : un joueur va dans un casino et veut optimiser ses gains sur des machines à sous, surnommées bandits manchots \footnote{Notons que "bandits manchots" est traduit par "multi-armed bandits" en anglais}. Pour cela, il doit trouver un compromis entre la détermination expérimentale des lois des différentes machines (nécessité d'exploration) et le temps passé à jouer pour récupérer les gains de la meilleure des machines (exploitation).

		Les nombreuses applications de ce problème pour le choix de l'affichage des publicités sur internet, les choix des traitements cliniques ou encore des routages de paquets \cite[page 3]{DBLP:journals/corr/abs-1204-5721}, et les enjeux financiers importants qu'ils représentent en font un domaine assez étudié. 

		Ce problème admet de nombreuses variantes et possède de nombreux paramètres : les  types de lois suivies par les machines, le nombre de machines (deux, fini ou infini) \cite[page 108]{DBLP:journals/corr/abs-1204-5721}. On peut aussi autoriser les lois à changer au cours du temps, voire en fonction des choix du joueur (adversarial bandits \cite[chapitre 3]{DBLP:journals/corr/abs-1204-5721}). Enfin, on peut disposer d'informations complémentaires pour orienter la prise de décision \cite[chapitre 4]{DBLP:journals/corr/abs-1204-5721}

		On peut décrire rapidement un cadre possible pour fixer les idées. Dans le cas d'un placement de publicités, les différentes machines possibles seront les publicités que l'on peut choisir d'afficher. La récompense sera le clic d'un utilisateur, voire un partage sur un réseau social par exemple. On voit que dans cet exemple la loi évolue au cours du temps : supposons que quelqu'un compte acheter une montre. Il a une plus grande probabilité de cliquer sur toutes les annonces qui en proposent. De plus, une annonce qui a déjà été visitée devient a priori beaucoup moins attractive. Enfin, une fois que la personne a commandé une montre, elle ne s'intéresse plus à de telles annonces publicitaires, donc il y a une rupture dans les probabilités correspondantes.



		Cependant, dans toutes ces variantes, la spécificité de ce problème reste le fait que l'information dont on dispose dépend de notre stratégie d'exploration, qui se fait a priori au détriment du temps passé à l'exploitation du bras considéré comme le meilleur connu. \cite[pages 19-32]{Sutton1998ReinforcementLA}. C'est le dilemme d'exploration-exploitation, que l'on retrouve fréquemment en apprentissage.




	\subsection{Formalisation mathématique du cas étudié}

		Dans le cadre de mon stage, on se limite aux bandits stochastiques, avec $K$ bras (i.e machines à sous). On peut résumer le déroulement général d'un algorithme de bandits manchots par le pseudo code suivant :

		\begin{algorithm}[H]
		\SetAlgoLined
		\KwData{\textit{known:} number of arms K, number of trials n

		\textit{unknown:} distribution of each arm}

		\For{trial in trials}{
		The player chooses an arm\;
		The arm gives the reward according its distribution\;
		The player updates his beliefs\;

		}
		\caption{Déroulement standard d'un problème de bandits manchots}
		\end{algorithm}

		En particulier, la récompense obtenue une fois le bras choisi ne dépend que de la loi du bras, et non pas de l'instant du tirage, ni du comportement des autres bras.

		Vient ensuite la question de l'évaluation de l'algorithme. Pour cela, on introduit la notion de regret, qui est la différence entre ce que le joueur a obtenu lors de l'expérience avec ce qu'il pouvait obtenir en choisissant à tout moment le meilleur bras possible. On note pour le $i$-ème des $K$ bras au temps $t$ l'espérance $\mu_{i,t}$ et $\mu^*_t = \max  \mu_{i,t}$. Alors, on a la définition suivante :

		\begin{defn}
		Le regret est la différence entre le gain du joueur et le gain théorique possible :
		\[ R_n = \sum_{t=1}^{n} \mu^*_t - \sum_{t=1}^{n} \mu_{I_t, t}  \]
		 $I_t$ est le bras choisi au temps $i$
		\end{defn}

		On remarque que si la loi ne change pas au cours du temps, cas que l'on étudie pour l'instant, on peut simplifier les notations et remarquer qu'il suffit de prendre $n$ fois le meilleur bras, d'où $R_n = n \times \mu^* - \sum_{t=1}^n \mu_{I_t}$ 

		Cependant, le regret est sensible aux fluctuations des différents tirages. Dans le cas de lois avec de grandes variances, la différence de performance de différents algorithmes peut être cachée par cette variation. On introduit donc le pseudo-regret, qui considère l'espérance de gains avec la stratégie qui a été jouée :

		\begin{defn}
			Le pseudo-regret est la différence entre l'espérance théorique optimale de récompense, et l'espérance de récompenses obtenues via la stratégie choisie, c'est-à-dire 
			\[R_n = n \mu^* - \Ex \sum_{t= 1}^{n} \mu_{I_t}  \]
		\end{defn}


		On peut alors considérer que plus le pseudo-regret est petit, plus l'algorithme est performant. On peut pour cela ou bien voir le pseudo-regret comme une fonction du temps, ou bien uniquement le pseudo-regret à la fin de l'expérience \cite{DBLP:journals/corr/KuleshovP14}.

	\subsection{Algorithmes standards}


		Plusieurs algorithmes apportent une réponse au problème des bandits \cite{DBLP:journals/corr/KuleshovP14}, avec un fonctionnement plus ou moins élaboré. Une stratégie très simple consiste à ne faire que de l'exploration sur une certaine proportion des essais, puis à ne jouer que le meilleur bras ensuite.

		Je me suis intéressée principalement à trois algorithmes connus pour résoudre ce problème, à savoir la stratégie epsilon-glouton, celle de Boltzmann et l'algorithme UCB (Upper Confidence Bound), que je vais décrire dans la suite de cette section.

		Les choix dépendent des moyennes $\mu_i$ des récompenses empiriquement calculées pour chaque bras, car on considère que ces moyennes reflètent la loi du bras, c'est le principe d'\emph{optimisme face à l'incertitude} \footnote{optimism in face of uncertainty}.


		La première façon pour établir un compromis entre l'exploration et l'exploitation est de jouer le meilleur bras connu avec une certaine probabilité, et aléatoirement le reste du temps : c'est la stratégie $\varepsilon$-greedy.

		Un autre équilibre est obtenu avec une probabilité de Boltzmann, qui favorise les meilleurs bras sans complètement arrêter de pouvoir jouer les autres. Pour cela, la probabilité pour un bras d'être joué est proportionnel à $e^{\frac{\mu}{\tau}}$ où $\tau$ est un paramètre de l'algorithme qui désigne la température de Boltzmann : si la température est très élevée, on tend à jouer chaque bras avec équiprobabilité, si elle est faible on ne joue que le bras considéré comme étant le meilleur.

		Enfin, l'\textit{upper confidence bound} permet de prendre en compte la fiabilité de l'estimation sur chaque moyenne, et en privilégiant donc à la fois le meilleur bras, mais aussi les bras les moins testés. Pour cela, on choisit le bras maximisant la quantité
		\[ \mu_i + \sqrt{ \frac{2 \log{t}}{  T_i(t)}} \]
		où $T_i(t) = \sum_{s=1}^{t} \onebb_{I_s = i}$.

		Pour le problème des bandits stochastiques, où le comportement du bandit n'évolue pas selon les actions sélectionnées par le joueur, il est encore possible d'ajouter quelques raffinements à l'algorithme UCB mais la performance est déjà assez élevée : l'objet n'était pas ici de refaire les démonstrations assez lourdes de probabilités qui assurent la pertinence de cet algorithme.


		En résumé, les trois algorithmes offrent trois possibilités pour sélectionner le bras à jouer. En notant $p_i(t)$ la probabilité de choisir le bras $i$ au temps $t$, on a :
		\begin{description}
	    \item [Epsilon-greedy] $p_i(t+1) = \left\{
	                \begin{array}{ll}
	                  1 - \varepsilon + \varepsilon / k  & \text{if } i = \argmax \mu_j(t)\\
	                  \varepsilon / k  & \text{sinon}
	                \end{array}
	              \right.$
	    \item [Bolzmann] $p_i(t+1) \propto e^{\mu_i(t) / \tau}$ 
	    \item [UCB] Le bras choisi est $\argmax \left( \mu_i + \sqrt{\frac{2 \ln t}{n_i}} \right)$ 
	  \end{description}

	  	 D'autres algorithmes sont possibles, notamment pour chaque variante possible du problème des bandits manchots, avec des résultats théoriques plus ou moins développés. Ainsi le Thomson sampling parvient à d'excellents résultats expérimentaux mais fait intervenir la loi bêta, complexe à analyser.



	

	\begin{comment}
	A nouveau tout ce que j'ai lu, algo...
	Les algos de bandits \cite{DBLP:journals/corr/KuleshovP14}
	Le livre sur les bandits \cite{DBLP:journals/corr/abs-1204-5721}
	Les rats \cite{article}
	L'article matheux sur les rats \cite{Lloyd20130069}
	Sur Ach et NE \cite{Yu:2002:EUU:2968618.2968640}
	\end{comment}


	
