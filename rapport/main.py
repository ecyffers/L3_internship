import numpy as np

def sinus(x):
    return np.sin(x*2*np.pi)

if __name__ == '__main__':
    import matplotlib
    import matplotlib.pyplot as plt
    from generation import gen_samples_proba
    import mlp
    import relevance

    goal = sinus
    drawing = False

    np.random.seed(1)


    def train(period, samples, outlier, batch,lrate=0.05, show_error =False, threshold=0.1, detection=False, factor = 10):
        '''Train the network. mistakes is the proportion of false points, show_error draw the error at each period
        The threshold is the naive way to choose outliers, if the detection is chosen, 
        we create an more sophisticated option of detection'''
        true_detection=0
        false_detection=0
        unseen=0
        mem_outlier ={}
        if detection :
            print "The detection is on"
        #Draw of the function used by the network
        x,y = samples['x'],samples['y']

        #in order to see the convergence
        evoerror = []

        #a first point to initialize moyerror and moydeltas
        network.propagate_forward(samples['x'][0])
        error, deltas=network.propagate_backward(samples['y'][0])
        moyerror = np.array(error)
        moydeltas = np.array(deltas)
        moy = 1

        for i in range(period):
            moyerror = 0
            moydeltas = 0
            for j in range(batch):
                n = np.random.randint(samples.size)
                network.propagate_forward(samples['x'][n])
                error, deltas=network.propagate_backward(samples['y'][n])
                if detection  :
                    nb_detection = true_detection + false_detection
                    true_detection,false_detection,unseen,moy=relevance.mustbeanoutlier(n,error,moy,outlier,true_detection,false_detection,unseen, factor)
                    #true_detection,false_detection, unseen=relevance.isanoutlier(n,error, outlier, true_detection,false_detection, unseen, threshold)
                    if nb_detection < true_detection + false_detection :
                        mem_outlier[n]=samples['y'][n]
                        moyerror-=np.array(error)
                        moydeltas-=np.array(deltas)

                moyerror+=np.array(error)
                moydeltas+=np.array(deltas)

            
            evoerror.append(abs(float(moyerror)))          
            
            network.updating(moyerror, moydeltas,lrate)
        
        if period > 1 and show_error:
            x,y = [i for i in range(period)], evoerror
            if detection:
                plt.plot(x,y, color = 'g',lw = 0.5)
            else :
                plt.plot(x,y, color = 'r',lw = 0.5)




        


        #About the detecton of outliers
        if detection:
            relevance.stats_outlier(outlier, period,batch, true_detection, false_detection,unseen)
            return mem_outlier
        

    def learn(exact_period, exact_points, mixed_period=0, mixed_points=0, mistakes=0, batch=1, lrate=0.05,show_error=False, factor = 10):
        '''exact period : number of batches with exact points,
        exact_points: number of exact points
        mixed period : number of batches with mixed points,
        mixed_points: number of mixed points '''
        if show_error :
            plt.figure(figsize=(10,5))

        samples, outlier = gen_samples_proba(exact_points, goal, 0)
        train(exact_period, samples, outlier, batch,lrate,True)
        samples,outlier = gen_samples_proba(mixed_points, goal, mistakes)
        mem_outlier=train(mixed_period, samples, outlier, batch,lrate, show_error, 0.2,True, factor)

        #train(mixed_period, samples, outlier, batch,lrate, show_error, 0.2,False)
        #mem_outlier = {}
        #We check that we detect exactly the outliers.
        #print "voici les outliers"
        #print outlier
        #print "et les detectes"
        success = True
        if len(outlier)!=len(mem_outlier.keys()):
            success = False
        else :
            for cle in outlier:
                if mem_outlier.get(cle, 1000)==1000:
                    success = False
        return samples, mem_outlier, success
    


	network = mlp.MLP(1,9,9,1)

	#samples, mem_outlier = learn(50000, 1000,0,3,0,1,0.02, True)
	samples, mem_outlier, success=learn(0, 2000,500000,2000,0.01*a,20,0.01, True, 20)

	erreurs = relevance.quadratic_error(network, samples, mem_outlier)

	print 'global error %.2f' % erreurs
	print success

	if drawing :
	    plt.figure(figsize=(10,5))

	    #drawing of the real sinus function
	    #samples = np.zeros(500, dtype=[('x',  float, 1), ('y', float, 1)])
	    #samples['x'] = np.linspace(0,1,500)
	    #samples['y'] = goal(samples['x'])
	    x,y = samples['x'],samples['y']
	    plt.scatter(x,y,s=10,color='turquoise')

	    # Draw network approximated function
	    for i in range(samples.shape[0]):
		y[i] = mem_outlier.get(i, 1000)
		if y[i]==1000:
		    y[i] = float(network.propagate_forward(samples['x'][i]))
	    plt.scatter(x,y,color='r',lw=1, marker ='+')

	    plt.show()

                
