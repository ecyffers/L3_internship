#Define the class with the different algorithmes to solve the contextual bandits problem
import numpy as np 

class Player(object):
	def __init__(self, nb_arm):
		self.nb_arm = nb_arm
		self.time = 0
		self.means = np.zeros(self.nb_arm)
		self.chosen = np.zeros(self.nb_arm)

	def reset(self, zero = False):
		self.chosen = np.zeros(self.nb_arm)
		self.means = np.random.normal(0.5, 0.01,self.nb_arm)
		if zero :
			self.means = np.zeros(self.nb_arm)
		self.time = 0

	def selection(self):
		raise NotImplementedError()

	def believe_and_select(self):
		raise NotImplementedError()

	def update(self, chosen_arm, reward):
		'''Update the data used by the algorithm 
		according to the result of the last arm pushed'''
		self.chosen[chosen_arm]+=1
		old = self.means[chosen_arm]
		n = self.chosen[chosen_arm]
		#The new empirical means : 
		self.means[chosen_arm] = (old*(n-1) + reward)/n
		self.time+=1


class Epsgreedy(Player):
	'''class for the epsilon greedy algorithm'''

	def __init__(self, epsilon, *args, eps_fun = None):
		super().__init__(*args)
		self.epsilon = epsilon
		self.eps_fun = eps_fun

	def selection(self):
		if np.random.uniform(0,1)<self.epsilon:
			arm = np.random.randint(self.nb_arm)
		else:
			arm = np.argmax(self.means)
		return arm

	def believe_and_select(self):
		arm = Epsgreedy.selection(self)
		return self.means[arm], arm

	def update(self, *args):
		super().update(*args)
		#If needed, the new epsilon
		if self.eps_fun is not None :
			self.epsilon = self.eps_fun(self.epsilon, self.time)


class Boltzmann(Player):
	'''Class for the boltzmann exploration,
	also known as softmax'''
	def __init__(self, tau, *args, tau_fun = None):
		super().__init__(*args)
		self.tau = tau 
		self.tau_fun = tau_fun

	def selection(self):
		proba = np.exp(self.means.copy()/self.tau)
		norm = proba.sum()
		proba/=norm

		arm =  np.random.choice([i for i in range(self.nb_arm)], p=proba)
		return arm

	def believe_and_select(self):
		arm = Boltzmann.selection(self)
		return self.means[arm], arm

	def update(self, *args):
		super().update(*args)
		if self.tau_fun is not None :
			self.tau = self.tau_fun(self.tau, self.time)


class UCB(Player):
	'''class for the Upper confident bound algorithm'''

	def reset(self):
		super().reset(zero = True)


	def selection(self):
		if self.time < self.nb_arm :
			return self.time
		else:
			to_play = self.means.copy()
			to_play+= np.sqrt(2*np.log(self.time)/self.chosen)
			return np.argmax(to_play)

	def believe_and_select(self):
		arm = UCB.selection(self)
		return self.means[arm], arm

	def update(self, chosen_arm, reward):
		if self.time < self.nb_arm:
			self.means[chosen_arm]=reward
			self.time+=1
			self.chosen[chosen_arm]=1
		else:
			super().update(chosen_arm, reward)

class Reinforcement(Player):
	'''class for a reinforcement strategy'''
	def __init__(self, alpha, *args, alpha_fun = None):
		super().__init__(*args)
		self.alpha = alpha
		self.alpha_fun = alpha_fun
		self.proba = np.array([1/self.nb_arm]*self.nb_arm)

	def selection(self):
		return  np.random.choice([i for i in range(self.nb_arm)], p=self.proba)

	def believe_and_select(self):
		arm = Reinforcement.selection(self)
		return self.means[arm], arm

	def update(self, chosen_arm, reward):
		super().update(chosen_arm, reward)
		max_reward = max(self.means)
		if reward > max_reward - .01:
			delta = self.alpha *(1-self.proba[chosen_arm])
		else :
			delta = self.alpha*(0 - self.proba[chosen_arm])
		self.proba-= delta/(self.nb_arm-1)
		self.proba[chosen_arm]+= delta *(1+1/(self.nb_arm-1))



