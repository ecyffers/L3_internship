Mon stage a aussi été l'occasion de découvrir le monde de la recherche et de me confronter plus directement aux problèmes d'éthique via les conférences auxquelles j'ai assistées, et les discussions que j'ai pu avoir avec les autres chercheurs. J'avais rédigé mes impressions à ce sujet, et je les ai communiquées à mon tuteur. Je présente donc ici une version raccourcie des questions qui m'ont interpellées pendant mon stage.


\subsection{Communication et vulgarisation}

	Le chercheur est souvent imaginé seul, fou dans sa tour d'ivoire. Cette image m'a toujours semblée injuste, car il n'y a pas de raison a priori pour que ce métier, où on voyage davantage que dans bien des métiers, et où il est crucial d'aller chercher du savoir auprès d'autres chercheurs soit un symbole d'isolement. Cependant, si les interactions sont fortes entre pairs, mon stage m'a confrontée à la rupture avec l'extérieur, et ce à plusieurs niveaux.

	L'institut des maladies neurodégénératives compte de nombreux biologistes, et la première difficulté de communication que j'ai pu observer était due à cette multi-disciplinarité. En effet, il n'est pas rare que l'on demande dans ce domaine aux informaticiens de traiter les données obtenues par les biologistes pour qu'ils puissent ensuite les interpréter. Le fonctionnement le plus basique est alors d'établir une liste des traitements attendus, puis de fournir une boîte noire au biologiste qui correspond au cahier des charges qu'il a établi. Cependant cette technique présente de nombreux problèmes. Tout d'abord, les biologistes n'ont pas forcément une idée claire du traitement qu'ils souhaitent, ni de la significativité des résultats qu'ils vont obtenir : par exemple le niveau de bruit de leur expérience n'est pas forcément estimable à l'avance. Construire un vrai dialogue, en identifiant les difficultés liés aux données qu'ils fournissent est donc un vrai challenge. Par ailleurs, il peut arriver qu'un prétraitement très simple soit demandé parce que la forme de l'enregistrement a été modifié par rapport à l'entrée de la boîte noire que l'on a fournie. Il peut arriver qu'un biologiste recopie alors des valeurs à la main, ou tronque ses données pour qu'elles correspondent à la forme attendue, ce qui donne une exploitation non optimale de ses résultats.

	Le compromis est bien sûr difficile à trouver entre fournir une boîte noire, qui a un temps d'apprentissage initial nul mais qui peut rester ensuite sous-optimale et une explication complète des traitements utilisés, trop coûteuse en temps voire impossible car trop éloignée du domaine du biologiste. On retrouve finalement un dilemme d'exploration exploitation similaire au problème des bandits.

	\paragraph{}
	Le problème du grand public me semble encore plus délicat à résoudre. En effet, l'informatique a complétement changé d'échelle en une génération, et il y a une énorme rupture entre l'imagination et le savoir du grand public et l'avancée actuelle de la recherche. Par exemple, lors d'une table ronde sur l'éthique, un chercheur nous a raconté qu'il avait récemment participé à une rencontre avec le grand public sur l'intelligence artificielle et ses dangers : plus de la moitié des questions concernaient l'installation du compteur Linky. Cet exemple me semble particulièrement illustrer le décalage complet entre ce que le public croit comprendre via la vulgarisation voire le complotisme qu'il rencontre et la réalité de la recherche.

	Dès lors, améliorer la vulgarisation est nécessaire si on veut créer un dialogue sur ces questions. L'équipe Mnémosyne participe à la fête de la Science : une discussion sur d'éventuels posters a été menée pendant mon stage. Ce n'est qu'un des exemples possibles de leur travail de vulgarisaton. L'INRIA organise ainsi de nombreux travaux \footnote{disponibles sur \url{https://www.inria.fr/recherches/mediation-scientifique}}, car d'autres sont moins divertissants et peut-être plus à même d'informer le citoyen et non uniquement de l'intriguer.


\subsection{Expérimentation animale}

	L'institut des maladies dégénératives comporte plusieurs équipes qui utilisent plus au moins l'expérimentation animale, notamment sur les rats, les oiseaux, les animaux marins et les singes. Je n'ai pas été en contact direct avec les animaux dans le cadre de mon stage, mais j'ai pu suivre une présentation pendant une demi-journée des expériences en cours sur des diamants mandarins, et plus généralement discuter avec des chercheurs tuant des animaux dans le cadre de leurs travaux ou les utilisant lors d'expérience.

	Il me semble que cette utilisation n'est pas évidente à gérer, et que les chercheurs gagneraient à être plus accompagnés dans ce domaine. Plusieurs stratagèmes semblent être mis en place pour supporter ce choix difficile, à la fois avec un grand détachement des cobayes, un vocabulaire particulièrement technique et le respect des protocoles d'éthique.

	Il semblerait que la situation se soit améliorée durant les dernières décennies. \footnote{comme on peut le voir sur le site \url{https://www.recherche-animale.org/decouvrir-la-recherche-animale/lethique-de-la-recherche}}, et nous pouvons donc espérer qu'elle continuera en ce sens. Thierry Vieville, un chercheur de l'équipe avait ainsi commencé une procédure d'adoption de singes retraités de laboratoire.



\subsection{Convictions actuelles}

	J'ai l'impression que la situation actuelle, où l'on se réjouit de la mise en place de protocoles, de comité d'éthique et de conférence sur ce sujet qui garantissent un respect croissant de l'animal et de l'individu est intéressante, mais qu'il est important de garder un regard critique. Il me semble qu'un risque est de se décharger de sa responsabilité sur ceux qui écrivent ces normes. Et même le chercheur est censé participer indirectement à leur écriture, cette participation me semble actuellement très difficile, tant le décalage entre les deux mondes est énorme.

	Une autre limite à surveiller est le risque de ne s'intéresser qu'au dernier maillon le plus appliqué, sans considérer toute la recherche théorique en amont. Pourtant, si celle-ci est moins visuelle, elle est bien souvent à l'origine. En découpant les problèmes d'éthique en sections et protocoles, il me semble qu'on peut plus facilement oublier la responsabilité globale de la recherche.

	Enfin, elle me renforce dans la conviction de l'utilité d'une formation philosophique, qui est généralement le parent pauvre du cursus scientifique en France, et qui pourtant me semble être un outil crucial pour aborder ces problèmes.