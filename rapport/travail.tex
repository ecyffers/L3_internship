 % Une ou plusieurs sections présentant le travail que vous avez effectué, vos
 % contributions ainsi que les difficultés rencontrées.

\subsection{Résumé du déroulement et objectifs}
	
	L'objectif de mon travail était dans un premier temps d'implémenter les différents algorithmes de bandits et de le tester avec différents paramètres, afin d'avoir un cadre pour, dans la suite, introduire facilement différents contextes, selon le sens précisé dans la partie 2. \footnote{Les codes sont en annexe D}

	L'étape suivante devait consister à essayer de retrouver par une modélisation les résultats de l'article de Brunswik \cite{article} dans la lignée du travail effectué par Kevin Lloyd and David S. Leslie \cite{Lloyd20130069}, qui met en évidence que l'animal ne désapprend pas à chaque fois pour réapprendre, mais qu'il est capable de passer d'un contexte à l'autre. Finalement, en étudiant le protocole expérimental utilisé, j'ai remarqué qu'il ne correspondait pas exactement à un problème de bandits manchots.

	J'ai étudié un article sur lequel mon tuteur a travaillé \cite{Shivkumar196543} afin de mieux comprendre l'objectif de la modélisation et de voir comment la formaliser. Finalement, je n'ai pas repris la formalisation assez complexe et pas entièrement satisfaisante de l'article \footnote{page 4, la simplification entre les équations 3 et 4 n'est pas évidente, et les calculs sans celle-ci deviennent ingérables à mon niveau} pour me concentrer sur l'implémentation des algorithmes de changements.

	Enfin, nous avons trouvé intéressant un lien qui me semblait apparaître entre une bonne adaptation des algorithmes et leur mauvaise détection des changements de contextes, j'ai donc cherché des résultats plus précis en ce sens.

\subsection{Choix de modélisation}
	
	L'objectif de ce travail est de mettre en avant deux niveaux d'incertitude qui peuvent se retrouver dans la vie animale. Un premier niveau d'incertitude qui fait partie du contexte et un deuxième qui se situe au niveau de l'appartenance au contexte.

	La difficulté de cet apprentissage vient de la stochasticité de la récompense. Par conséquent, il y a une difficulté inhérente à identifier la cause d'une absence d'une récompense : soit stochasticité à l'intérieur du contexte, soit changement de contexte.

	Un exemple concret est celui d'un oiseau qui chasse dans un cadre donné. Dans le cadre qu'il a identifié, il a une certaine probabilité d'attraper une proie. Mais cette probabilité peut changer drastiquement si le contexte change, par exemple si le vent tourne. Il doit donc prendre des décisions en considérant à la fois ce qu'il sait sur le contexte qu'il a identifié, et sur la fiabilité des informations qu'il a perçues, en se plaçant au besoin dans un autre contexte s'il se rend compte que les informations qu'il découvre ne correspondent pas au cadre qu'il avait identifié. Ces deux types d'incertitude pourraient être présentes dans le cerveau via le fonctionnement de deux hormones Ach et Ne \cite{Yu:2002:EUU:2968618.2968640}.


	\paragraph{}Le but est de traduire cela en en problème de bandits aussi simple que possible. Pour cela nous ne considérons que deux bras, car il est souvent difficile de faire plus expérimentalement. Les bras suivent des lois de Bernoulli. En effet, les expériences donnent une récompense sous forme de nourriture, faire de petites variations avec une loi normale ne serait pas détecté a priori par l'animal. 

	Un changement de contexte est alors un changement des paramètres des lois des deux bras. 

	Enfin, le but était uniquement de détecter un changement de contexte, et non pas de savoir reconnaître un contexte pour l'instant. Pour cette deuxième tâche, il faudrait développer un autre algorithme permettant de reconnaître un contexte. Nous avons, au contraire, décidé de n'avoir que deux contextes : si on pense que le contexte a changé, on se place dans l'autre contexte.

	Enfin, nous avons utilisé un calcul exacte des moyennes de gains des différents bras. Cette modélisation n'est pas satisfaisante d'un point de vue strictement biologique : une moyenne pondérée sur un nombre restreint de termes par exemple est jugée généralement plus plausible, mais cette implémentation permettait de reprendre au besoin les résultats théoriques connus via les algorithmes utilisés.

\subsection{Premiers résultats}

	\subsubsection*{Sans changement de contextes}

	J'ai commencé par coder les trois algorithmes classiques pour résoudre les bandits manchots stochastiques, avec deux bras. Un premier cas très simple est celui où l'un est toujours accompagné d'une récompense tandis que l'autre jamais. Dans ce cas, comme le résultat d'un bras est déterministe, les algorithmes convergent très vite. En faisant la moyenne sur 1000 essais avec 100 coups à jouer, on observe en particulier qu'UCB est bien déterministe dans ce cas, et qu'il va aller améliorer de moins en moins souvent sa connaissance du bras perdant. Par ailleurs, epsilon-greedy stagne vers 95\% de réussite pour un $\epsilon = 0.1$  car 10\%  des essais, il choisit un bras au hasard et a alors une chance sur deux de jouer le bras gagnant.

	\begin{figure}[ht]
		\begin{center}
		\includegraphics[height=7cm]{base.png}
		
		\caption{Les trois algorithmes de bandits manchots dans un cas très simple}
		\end{center}
	\end{figure}

	On peut ensuite prendre d'autres probabilités de récompense pour les deux bras, comme fait dans la figure 3.

	\begin{figure}
		\begin{center}
		\includegraphics[height=7cm]{uncontexte.png}
		
		\caption{Les trois algorithmes de bandits manchots dans un cas plus général}
		\end{center}
	\end{figure}

	Ici, j'ai à nouveau pris la moyenne de 5000 cas, et j'ai choisi la température pour Boltzmann et le epsilon pour espilon greedy de façon à optimiser leur résultat. On observe plusieurs phénomènes intéressants. Tout d'abord, même si UCB est le "meilleur" algorithme théoriquement, il peut se faire battre par epsilon-greedy et Boltzmann dès lors qu'on choisit les paramètres de ceux-ci en fonction des lois auxquelles on est confronté : ici peu d'exploration suffit. Ensuite, la variance des trois algorithmes est élevée, car les décisions dépendent de l'aléatoire des bras. Pour obtenir une courbe significative, il est donc nécessaire de faire la moyenne sur de nombreux cas. 

	\subsubsection*{Avec deux contextes sans prévoir le changement}

	On introduit maintenant la possibilité d'un changement de contexte : à certains instants $t$, les distributions de probabilité des deux bras sont modifiées. Par souci de simplification, un seul cas a été étudié lors de mon stage, à savoir les lois des deux bras sont échangées.

	Si on effectue cette opération sans introduire la possibilité de réinitialiser l'apprentissage ou de gérer deux contextes dans le joueur, on obtient la courbe de la figure 4.


	\begin{figure}[ht!]
		\begin{center}
		\includegraphics[height=7cm]{comparaison.png}
		
		\caption{Changement de contextes sans modification des trois algorithmes de bandits manchots}
		\end{center}
	\end{figure}

	On observe que ces courbes ne sont pas satisfaisantes : il est nécessaire de commencer par désapprendre ce que l'algorithme a appris pour pouvoir réapprendre ensuite. Le fait de présenter plusieurs fois le même contexte ne permet pas d'amélioration sur celui-ci car la connaissance accumulée lors des essais précédents a été détruite par le changement de contexte.

	\paragraph{}Ce comportement n'est de plus pas conforme à celui observé chez les animaux, qui arrivent à mémoriser de telles connaissances, par exemple en comparant avec une expérience assez similaire \footnote{on doit  néanmoins noter une différence par rapport à une vraie expérience de bandits manchots, car le rat devait toujours explorer le bras gagnant si celui-ci existait, ce qui favorise un changement rapide : "In those cases in which no reward was given on one of the two sides (see below), the door
	to the food compartment at the end of the respective path was closed and the animal had to turn and go to the other door. In each trial the animal finally obtained food.""} de Brunswik \cite{article}.

	\begin{figure}[ht]
	\begin{center}
		\includegraphics[height=10cm]{brunswik.png}
		
		\caption{Figure extraite de l'article de Brunswik sur le nombre d'erreurs commises par des rats devant choisir entre deux bras}
	\end{center}
	\end{figure}

	\subsubsection*{Détection grâce aux algorithmes CUSUM et Page}

	Pour améliorer les performances précédentes, on introduit des algorithmes de détection de changement. On crée au début de chaque simulation deux joueurs, on commence avec le premier des deux joueurs, puis on change de joueur à chaque fois qu'un changement de probabilité est détecté.

	Ainsi, en reprenant l'expérience précédente, avec l'ajout de l'algorithme CUSUM avec un faible seuil de détection, on peut obtenir de très bonnes performances.

	\begin{figure}[ht]
	\begin{center}
		\includegraphics[height=7cm]{cusum1.png}
		
		\caption{Les trois algorithmes de bandits manchots couplés avec l'algorithme CUSUM}
	\end{center}
	\end{figure}

	\begin{figure}[ht]
	\begin{center}
		\includegraphics[height=7cm]{cusum2.png}
		
		\caption{Une performance due à un bon choix de contexte : évolution de la proportion de la croyance en le bon contexte}
	\end{center}
	\end{figure}

	\paragraph{}
	Cependant, on a d'abord pris un cas très simple, car déterministe. Par conséquent la détection de changement est aussi très simple, puisqu'il suffit de ne pas autoriser d'erreur.

	On reprend donc l'expérience, avec cette fois-ci des lois non constantes. Les résultats sont alors bien moins bons, même lorsqu'on essaie d'optimiser le paramètre de seuil, comme on peut le faire en traçant le regret en fonction de celui-ci.

	\begin{figure}[ht]
	\begin{center}
		\includegraphics[height=7cm]{intuition.png}
		
		\caption{Évolution de la proportion de bons choix en fonction du paramètre de seuil}
	\end{center}
	\end{figure}

	Les résultats précédents ne diffèrent pas significativement lorsqu'on utilise l'algorithme de Page-Hinkley à la place de CUSUM.

	On observe qu'on a déjà un meilleur résultat que le hasard ou que celui de l'algorithme sans détection de changement. Mais on voit que le seuil dépend de la loi de probabilité (le résultat est très différent de celui avec les probabilités précédentes) et de l'algorithme utilisé. En effet, on remarque que l'algorithme UCB, qui a la meilleure faculté d'adaptation, est celui qui fonctionne mieux avec un seuil plus élevé, tandis que les deux autres sont plus réactifs au changement, et fonctionnent donc mieux avec un seuil plus faible.

	C'est cette constatation due à la recherche d'optimisation du seuil qui a semblée être intéressante : il faut trouver un équilibre entre une trop grande adaptabilité de type UCB qui permet de bons résultats, mais qui détecte lentement le changement, et une trop grande obstination qui n'apprend pas les différents contextes.



\subsection{Entre adaptabilité et détection}

	%Avec un contexte qui s'adapte : ADWIN
	%Voire les résultats avec cette absence de changement de contexte
	On décide de se restreindre à la stratégie epsilon greedy, avec la possibilité d'un $\varepsilon$ variable. En effet, puisqu'on ne considère que deux bras, la stratégie utilisée est entièrement définie par la probabilité de choisir le bras optimal à un instant donné. Par exemple, l'algorithme de Boltzmann avec une température $\tau$ correspond à un epsilon-greedy de paramètre $\varepsilon = \frac{2 e^{\mu_{opt}/ \tau}}{e^{\mu_{opt}/ \tau} + e^{\mu_{non-opt}/ \tau}}$.

	On peut alors regarder plus en détail comment se passe la détection du changement de contexte sur des cas particuliers.

	\begin{figure}[ht!]
	\begin{center}
		\includegraphics[height=10cm]{particulier.png}
		
		\caption{100 déroulements d'espsilon greedy avec des probabiltés de 80\% et 20\% de succès respectivements. Le moment de la détection de changement de contexte est entouré en rouge.}
	\end{center}
	\end{figure}

	On voit que la détection reste lente dans certains cas, voire inexistante, alors qu'il y a déjà  des fausses détections : il faudrait donc une variation de la taille d'epsilon en fonction de la stabilité du contexte. Ceci permettrait d'éviter trop de fausses détections quand le contexte est stable, tout en gardant un apprentissage rapide après un changement de contexte. Une idée évoquée mais pas encore testée serait d'utiliser une moyenne de la récompense utilisant l'algorithme ADWIN et d'avoir pour espsilon une fonction de la largeur de la fenêtre utilisée pour faire la moyenne.

	Dans tous les cas, on voit déjà apparaître les limites possibles de la détection avec des lois de Bernouilli : leur grande variance rend difficile l'attribution d'une non-récompense entre stochasticité du processus et changement de contextes.
	%Je me demande si ce n'est pas en fait équivalent à CUSUM ? L'accumulation d'erreur et la différence de moyenne peuvent s'exprimer l'une en fonction de l'autre, donc je pense qu'en fait on ne gagne rien avec cette méthode, contrairement à ce qu'on avait pu imaginer quand j'avais fait ma présentation ?

	%A la place je pourrai essayer de comparer avec les performances en faisant la moyenne sur ADWIN, même si c'est assez coûteux expérimentalement.

	%Ou alors je pourrais essayer de me concentrer sur une amélioration d'un des trois algorithmes, de préférence l'Epsilon-greedy, pour essayer de faire varier le epsilon astucieusement, je pense qu'on pourrait obtenir de bon résultat en faisant un epsilon fonction de la largeur de la fenêtre utiliser dans ADWIN.

	%Par ailleurs, dans le cas particulier que nous étudions Boltzmann et Epsilon-greedy sont équivalents, je pensais ajouter la preuve, et agir en conséquence.

	%A implémenter


